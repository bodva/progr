<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Blog extends Controller_Template
{

    public function action_index()
    {	
        $posts = ORM::factory('post')->order_by('id','DESC')->find_all();
        $this->template->content = view::factory('pages/index');
        $this->template->content->posts = $posts;
        // $this->template->content = 'Здесь список записей';
    }

    public function action_show(){
    	$id = (int)$this->request->param("eid");
    	$post = ORM::factory('post',$id);
    	$this->template->content = view::factory('pages/entry');
    	$this->template->content->post = $post;
    }

    public function action_add(){
    	if (Auth::instance()->logged_in()) {
	    	$post = ORM::factory('post');
	    	if (!empty($_REQUEST['save'])) {
	    		$post = ORM::factory('post')->set('title', $_REQUEST['title'])
	    							->set('body', $_REQUEST['body'])
	    							->set('author', Auth::instance()->get_user()->id)
	    							->save();
	    		$this->request->redirect('/');
	    	}

	    	$this->template->content = view::factory('pages/add');
	    	//$this->template->content->text = "Add new";
	    	$this->template->content->post = $post;
	    } else {
	    	$this->request->redirect('/');
	    }
    }

}

?>
