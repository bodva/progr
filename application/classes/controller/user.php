<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_User extends Controller_Template
{

    public function action_login()
    {
        if (isset($_POST['control']))
        {
            $user = Auth::instance()->login($_POST['username'], $_POST['password']);
            if ($user)
            {
                $this->request->redirect('/');
            }
        }
        $this->template->content = view::factory('pages/user/login');
    }

    public function action_logout()
    {
        Auth::instance()->logout();
        $this->request->redirect('/');
    }

}

?>
