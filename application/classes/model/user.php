<?php

class Model_User extends Model_Auth_User
{

    public function findIdByName($name)
    {
        $user = ORM::factory('user')->where('username', '=', $name)->find();
        if ($user->loaded())
            return $user->id;
        else
            return false;
    }

}

?>
