<?

return array(
    'database' => array(
        'name' => 'session',
        'encrypted' => FALSE,
        'lifetime' => 43200,
        'group' => 'default',
        'table' => 'sessions',
        'columns' => array(
            'session_id' => 'session_id',
            'last_active' => 'last_active',
            'contents' => 'contents'
        ),
        'gc' => 500,
    ),
);
