<?php defined('SYSPATH') or die('No direct access allowed.');

return array(

	'driver'       => 'ORM',
	'hash_method'  => 'sha256',
	'hash_key'     => 'XNULL',
	'lifetime'     => 1209600,
	'session_type' => 'database',
	'session_key'  => 'auth_user'
);
