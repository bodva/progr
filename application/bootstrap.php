<?php

defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------

require SYSPATH . 'classes/kohana/core' . EXT;

if (is_file(APPPATH . 'classes/kohana' . EXT))
    require APPPATH . 'classes/kohana' . EXT;

else
    require SYSPATH . 'classes/kohana' . EXT;


date_default_timezone_set('Europe/Kiev');
setlocale(LC_ALL, 'ru_RU.utf-8');
spl_autoload_register(array('Kohana', 'auto_load'));
ini_set('unserialize_callback_func', 'spl_autoload_call');
I18n::lang('ru');

Cookie::$salt = 'cb';

ini_set('session.gc_probability', 0);


if (isset($_SERVER['KOHANA_ENV']))
{
    Kohana::$environment = constant('Kohana::' . strtoupper($_SERVER['KOHANA_ENV']));
}



Kohana::init(array(
    'base_url' => '/',
    'index_file' => '',
));

//if (Kohana::$errors)
//{
//    set_exception_handler(array('HTTP_Exception', 'handler'));
//}

Kohana::$log->attach(new Log_File(APPPATH . 'logs'));

Kohana::$config->attach(new Config_File);



Kohana::modules(array(
    // 'cache'      => MODPATH.'cache',      // Caching with multiple backends
    // 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
    'database' => MODPATH . 'database', // Database access
    'image' => MODPATH . 'image', // Image manipulation
    'orm' => MODPATH . 'orm', // Object Relationship Mapping
    'auth' => MODPATH . 'auth', // Basic authentication
    // 'unittest'   => MODPATH.'unittest',   // Unit testing
    'userguide' => MODPATH . 'userguide', // User guide and API documentation
));

// ---------------------------------------------------------------
// -- [ Функции скрипта ] ----------------------------------------
// ---------------------------------------------------------------s


function xprint($param, $title = 'Отладочная информация')
{
    echo '
	<div style="padding:10px;margin-bottom:25px;color:black;background:#ededed;position:relative;top:18px;border:1px solid gray;font-size:11px;font-family:Tahoma;width:60%;">
		<div style="padding-top:1px;color:#000;background:#cccccc;position:relative;top:-18px;font-weight:bold;width:170px;height:15px;text-align:center;border:1px solid gray;">
			' . $title . '
		</div>
		<pre style=color:black;>' . htmlspecialchars(print_r($param, true)) . '
		</pre>
	</div>';
}

include APPPATH . '../environment.php';
$sess = Session::instance('database');


require 'router.php';