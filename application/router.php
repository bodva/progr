<?php

//-----------------------------------------------------
// -------[ Модуль юзеров ]----------------------------
//-----------------------------------------------------

Route::set('user/login', 'user/login')
        ->defaults(array(
            'controller' => 'user',
            'action' => 'login',
        ));

Route::set('user/logout', 'user/logout')
        ->defaults(array(
            'controller' => 'user',
            'action' => 'logout',
        ));

//-----------------------------------------------------
// -------[ Модуль цитат ]-----------------------------
//-----------------------------------------------------

Route::set('add', 'add')
        ->defaults(array(
            'controller' => 'blog',
            'action' => 'add',
        ));

Route::set('entry', 'entry(/<eid>)')
        ->defaults(array(
            'controller' => 'blog',
            'action' => 'show',
        ));

//-----------------------------------------------------
// -------[ Базовая маршрутизация ]---------------------
//-----------------------------------------------------


Route::set('', '')
        ->defaults(array(
            'controller' => 'blog',
            'action' => 'index',
        ));

Route::set('default', '(<controller>(/<action>(/<id>)))')
        ->defaults(array(
            'controller' => 'welcome',
            'action' => 'index',
        ));