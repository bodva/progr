<html>
    <head>
        <title>Progr - программистский цитатник</title>
        <?=View::factory('blocks/header')?>
    </head>
    <body>
        <div id="progr-header">
            <span id="site-name"><a href="<?php echo url::base(); ?>">Progr</a> - программистский цитатник</snap>
            <span class="userblock"><?=View::factory('blocks/userblock')?></span>
        </div>

        <div id="progr-content">
            <?= $content ?>
        </div>

        <div id="progr-footer">
            Сайт разработан креативной группой ZB Team в 2012 году. Спасибо разработчикам Kohana Framework<a href="<?php echo url::site("add"); ?>">.</a>
        </div>
    </body>
</html>