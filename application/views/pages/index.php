<?php
foreach ($posts as $post)
{
    ?>
    <div class="entry">
        <p class="entry-header"><a href="<?php echo url::site('entry/'.$post->id) ?>"><?php echo $post->title; ?></a></p>
        <p class="entry-body"><?php echo nl2br($post->body); ?></p>
        <p class="entry-dateline"><?php echo ORM::factory('user',$post->author)->username . ' - ' . $post->created; ?></p>
    </div>
<?php }
?>